<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header single-header alignwide">
		<div class="wrapper-single ">
			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
				<p class="meta-nav">
				<?php 
					$twentytwentyone_prev = is_rtl() ? ihag_get_icon_svg( 'ui', 'arrow_right' ) : ihag_get_icon_svg( 'ui', 'arrow_left' );
					_e($twentytwentyone_prev . "Retour aux articles" , 'ihag');
				?>					
				</p>

			</a>
			
		</div>

		<div class="">
			<p class="article-single">
				<span>
					<?php
					_e("Publié le ", 'ihag');
					echo get_the_date();
					?>				
				</span>	
			</p>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php ihag_post_thumbnail(); ?>			
		</div>

	</header><!-- .entry-header -->

	<div class="entry-content alignwide">
		<?php
		the_content();

		/*wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'ihag' ) . '">',
				'after'    => '</nav>',
				translators: %: Page number. 
				'pagelink' => esc_html__( 'Page %', 'ihag' ),
			)
		);*/
		?>
	</div><!-- .entry-content -->

	


</article><!-- #post-<?php the_ID(); ?> -->
